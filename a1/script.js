db.rooms.insertOne({
		name: "single",
		accomodates: 2,
		price: "1000",
		description: "a simple room with all the basic nesessities",
		room_available: 10,
		isAvailable: false
	});


db.rooms.insertMany([
		{ name: "double",
		  accomodates: 3,
		  price: 2000,
		  description: "a room fit for a smal family going on a vacation",
		  room_available: 5,
		  isAvailable: false
		  },
			{ name: "queen",
			  accomodates: 4,
			  price: 4000,
			  description: "a room with queen size bed perfect for a simple getaway",
			  room_available: 15,
			  isAvailable: false }
	]);

db.rooms.find({ name: "double"})



db.rooms.updateOne(
{name: "queen"},
	{
		$set:{
		room_available: 0
		}
	}
);


db.rooms.deleteMany({room_available: 0});
