// CRUD OPeration

/*Create, read, update, dalete
	-create: the create function allows users to create a new record in the database
	-read: the read function is similar to search function it allows user to search and retrieve specific records
	-update: the function is used to modify existing records that are in our database
	-delete: the delete function allow user to remove recods from database that is no longer needed

*/

// CREATE: INSERT Document
/*
	-the mongo uses JS for its syntax
	-MongoDB deals w/ objects as its sturcture for documents.
	-we can create documents by providing obbject into our methods
	-JavaScript Syntax:
		-Object.object.methods({object})
*/
// INSERT ONE
/*
	Syntax: 
	DB.collectionName.insertone({object})
*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: " 87654321",
		email: "janedoe@gmail.com"
	}
});

// insert Many
/*
	Syntax
		-db.collectionName.insertMany([{objectA},{objectB}]);
*/

db.users.insertMany([{
	firstName: "stephen",
	lastName: "hawking",
	age: 76,
	contact:{
		phone: "8888888",
		email: "stephenhawking@mail.com"

	},
	course:["Python","React","PHP"],
	departmen: "none"
},
{
	firstName: "Neil",
	lastName: "amrstrong",
	age: 82,
	contact:{
		phone: "665656",
		email: "neilstrong@mail.com"
	},
	course:["laraVel","React","Sass"]
	

}]);

// Read Find/retrieve documents
/*
	-the doc will be returned based on thier order of storage in th collection
*/
// find all document
/*
	Syntax: db.collectionName.find();
*/
db.users.find();

// find using single parameter
/*
	syntax
		Syntax: db.collectionName.find({field : value});
*/
db.users.find({firstName: "stephen"});
// find using multiple parameters
/*
	Syntax:
	- db.collectionName.find({fieldA:valueA, fieldB: ValueB});
*/
db.users.find({lastName: "amrstrong", age: 82});

// Find + pretty methods
/*
	the "pretty" methods allows us to be able to view the doc returned by our terminal in a "prettier" format.
		-syntax:
			-db.collection.find({field:value}).pretty();
*/
db.users.find({lastName: "amrstrong", age: 82}).pretty();

// update: EDIT A Document
// update one :updating asingle document
/*
	-syntax:
			-db.collection.updateOne({criteria}, {$set:{field:value}});
*/
// for our example, let us create a document that we will then update
// 1.insert initial document
db.users.insert({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact:{
		phone: "00000",
		email: "test@gmail.com"
	},
	courses:[],
	department: "none"
});

// 2.update the document
db.users.updateOne(
{firstName: "test"},
	{
		$set:{
		 firstName: "Bill",
		 lastName: "Gates",
		 age: 65,
		 contact:{
		 	phone: "87654321",
			email: "bill@gmail.com"
		 },
		 courses:["PHP","Laravel","HTML"],
		 department: "Operations",
		 status: "active"
		}
	}
);

db.users.find({firstName: "Bill"});

// updating many :updating Multiple documents
/*
	Syntax:
		-db.collection.updateMany( {criteria}, {$set: {field:value}} )
*/

db.user.updateMany(
{department: "none"},
	{
		$set: {
		department: "HR"
	}}
);


// db.users.deleteMany()

// Replace one
/*
	-replace one replace the document
	-if updateOne updates specific field , replaceOne replace the whole document

*/
db.users.replaceOne(
{firstName: "Bill"},
{
	firstName: "Bill",
	lastName: "Gates",
	age: 65,
	contact:{
		phone: "12345678",
		email: " bill@rocketmail.com"
	},
	courses : ["PHP","Laravel", "HTML"],
	department: "Operations"
}
);

//  DELETING: DELETING Documents
db.users.find({firstName: null});
db.users.deleteMany({firstName: null});

// Advance Queries
/*
	-retrieving data w/ complex data structures is also a good skill for any developer to develop
*/